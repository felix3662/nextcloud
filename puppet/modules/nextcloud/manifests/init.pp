/*
@Felix Alipaz
This file contains the RabbitMQ configuration
First 2 Files will be created that contain the erlang cookie
Depending which user is executing the rabbitmq-server command another file with the Erlang cookie will be read.
After that the Management plugin for RabbitMQ will be created
The file /etc/hosts will add Hosts declared by the Python programm to produce Vagrantboxes
The RabbitMQ-server service will be restarted
Finally the RabbitMQ Node will be added to the Cluster by joining the previous Node
*/

class nextcloud {
exec { 'Generate the config':
  command  => 'sudo mysql_secure_installation',
}


file  {'/root/default.sql':
        owner   =>  'root',
        group   =>  'root',
        ensure  =>  'file',
        mode    =>  '0755',
        content =>  template('nextcloud/default.sql')
    }


exec {"Default-SQL":
	command => "mysql -u root -proot < /root/default.sql",
}

exec { 'Download nextcloud':
  command => 'wget https://download.nextcloud.com/server/releases/nextcloud-20.0.0.zip -O
/tmp/nextcloud.zip',
}
exec { 'UNZIP nextcloud':
  command => 'unzip /tmp/nextcloud.zip -d /tmp/nextcloud',
}



exec {"MOVE NEXTCLOUD DATA":
	command => "mv /tmp/nextcloud /var/www/html/nextcloud",
}

exec {"Change owner of nextcloud":
	command => "chown -R www-data:www-data /var/www/html/nextcloud",
}

file {"/etc/apache2/sites-available/nextcloud.conf":
			owner => 'root',
			group => 'root',
			ensure => 'file',
			mode => '644',
			content => template('nextcloud/nextcloud.conf')
}

exec {"Enable apache2 nextcloud":
	command => "a2ensite nextcloud",
}

exec {"Enable apache2 modules":
	command => "a2enmode rewrite headers env dir mime",
}

exec {"Change php memory limit":
	command => "sed -i \'/^memory_limit =/s/=.*/= 512M/\' /etc/php/7.4/apache2/php.ini",
}
}

