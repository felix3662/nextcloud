class system_update {

package{['apt-transport-https']:
        ensure => 'installed',
}
/*
file {'/etc/apt/sources.list.d/bintray.rabbitmq.list':
        owner   => 'root',
        group   => 'root',
        ensure  => 'file',
        mode    => '644',
        content => 'deb https://packages.erlang-solutions.com/debian stretch contrib',
}

exec { 'add key for erlang':
        command => "wget -O- https://packages.erlang-solutions.com/debian/erlang_solutions.asc | sudo apt-key add -"
}
*/

exec { 'apt update':
command => 'apt update',
}

exec { 'apt upgrade':
command => 'apt upgrade -y'
} 


$sysPackages = [ "build-essential" ] package { $sysPackages:
ensure => "installed",
}

}
