/*
@Alipaz
*/
class packages {



    package { ['socat','python3-pip',
			   'vim','git','apache2',
			   'libapache2-mod-php',
			   'mariadb-server',
			   'php-xml', 'php-cli', 
			   'php-cgi', 'php-mysql', 
			   'php-mbstring', 'php-gd', 
			   'php-curl', 'php-zip',
			   'wget', 'zip', 'unzip',
				 'tmux','tmate',
				 'python3-samba','samba-common-bin',
         'samba-common', 'samba-dsdb-modules',
				 'samba-libs', 'samba-vfs-modules', 'samba',
				 'php', 'php-json', 'php-intl', 
				 'php-imagick', 'php-bcmath', 'php-gmp'
				

				
				

			  ] :
     ensure => 'installed',
    }
service { ["apache2"]: 
   provider => systemd,
   ensure => running,
   enable => true,
}

}
