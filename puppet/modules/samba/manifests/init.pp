/*
@Alipaz
*/
class samba {
file { '/etc/samba/smb.conf':
  owner => 'root',
  group => 'root',
  ensure=> 'file',
  mode  => '644',
  content => template('samba/smb.conf')
}

file { '/var/www/html/nextcloud/sergio/files/Scanner':
  owner => 'pi',
  group => 'www-data',
  ensure=> 'directory',
  mode  => '755',
}
service { ["smbd"]: 
   provider => systemd,
   ensure => running,
   enable => true,
}

}
