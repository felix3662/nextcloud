/*
@Felix Alipaz
This file contains the RabbitMQ configuration
First 2 Files will be created that contain the erlang cookie
Depending which user is executing the rabbitmq-server command another file with the Erlang cookie will be read.
After that the Management plugin for RabbitMQ will be created
The file /etc/hosts will add Hosts declared by the Python programm to produce Vagrantboxes
The RabbitMQ-server service will be restarted
Finally the RabbitMQ Node will be added to the Cluster by joining the previous Node
*/
class fzf{

$user = '/home/vagrant'

exec{'retrieve_fzf':
  command => "git clone --depth 1 https://github.com/junegunn/fzf.git ${user}/.fzf"
}


exec{'Replace home in install script':
  command => "sed -i s+\\\$HOME+/home/debian+g ${user}/.fzf/install"
}


exec{'Install_fzf':
  command => "yes | bash ${user}/.fzf/install"
}
}
