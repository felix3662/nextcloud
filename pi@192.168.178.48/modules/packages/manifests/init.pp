/*
@Alipaz
*/
class packages {
    package { ['socat','python3-pip',
			   'vim','git','apache2',
			   'libapache2-mod-php',
			   'mariadb-server',
			   'php-xml', 'php-cli', 
			   'php-cgi', 'php-mysql', 
			   'php-mbstring', 'php-gd', 
			   'php-curl', 'php-zip',
			   'wget', 'zip',
				

			  ] :
     ensure => 'installed',
    }
service { ["apache2"]: 
   provider => systemd,
   ensure => running,
   enable => true,
}

}
