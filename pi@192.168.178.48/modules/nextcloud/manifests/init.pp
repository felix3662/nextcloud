/*
@Felix Alipaz
This file contains the RabbitMQ configuration
First 2 Files will be created that contain the erlang cookie
Depending which user is executing the rabbitmq-server command another file with the Erlang cookie will be read.
After that the Management plugin for RabbitMQ will be created
The file /etc/hosts will add Hosts declared by the Python programm to produce Vagrantboxes
The RabbitMQ-server service will be restarted
Finally the RabbitMQ Node will be added to the Cluster by joining the previous Node
*/

class nextcloud {


exec{'retrieve_nextcloud':
  command => '/usr/bin/wget -q  https://download.nextcloud.com/server/releases/nextcloud-18.0.2.zip -O /tmp/nextcloud.zip',
}

exec{'Extract nextcloud':
  command => 'unzip -n /tmp/nextcloud.zip -d /tmp/nextcloud'
}
exec{'Copy html':
  command => 'sudo cp -r /tmp/nextcloud/* /var/www/html/nextcloud'
}

exec{'Chown ':
  command => 'sudo chown -R www-data:www-data /var/www/html/nextcloud'
}
}
